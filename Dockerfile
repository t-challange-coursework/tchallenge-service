FROM openjdk:8-jre

ADD ./source/build/libs/tchallenge-service.jar /app/app.jar

WORKDIR /app

EXPOSE 4567 

CMD ["java", "-jar", "/app/app.jar"]
